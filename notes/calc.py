 '''
 define a calculator class for other functions to use
 '''
 class Calculator:
    def __init__(self):
        self.num1 = 0
        self.num2 = 0
        self.result = 0

    def add(self, num1, num2):
        self.num1 = num1
        self.num2 = num2
        self.result = self.num1 + self.num2
        return self.result    

    def sub(self, num1, num2):
        self.num1 = num1
        self.num2 = num     
        
